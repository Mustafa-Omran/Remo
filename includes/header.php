    <head>
        <meta charset="UTF-8">
        <title>Detect me</title>
        
        <link href="css/style.css" rel="stylesheet">
        <!-- using w3.css library for responsive design-->
        <link href="css/w3.css" rel="stylesheet">
        <!-- using font awesome for icons-->
        <link href="font awesome/css/font-awesome.min.css" rel="stylesheet">
        
        <link rel="shortcut icon" href="images/icon.png" />
        
    </head>

