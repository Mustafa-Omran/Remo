
<!-- content -->    

<body class="w3-container w3-light-grey"> 
    
    <!-- sub header -->
        <div id="header" class="w3-orange w3-text-black w3-center w3-xlarge">
            
            <p> <i class=" fa fa-user w3-jumbo w3-hover-text-white"></i> Detect me</p>
            <sub class="w3-hover-text-deep-purple">Application for detecting user info</sub>
            
        </div>
    
    
    <!-- User info-->
        <div id="content" class="w3-border w3-border-orange">
            
        <p class="w3-center w3-text-black w3-xlarge">
            <span class=" fa fa-address-book"> IP Address : <?php echo $_SERVER['REMOTE_ADDR']; ?> </span> 
         <br>
         <span class="fa fa-navicon"> Port Number : <?php echo $_SERVER['REMOTE_PORT']; ?> </span>   
         <br>
         <span class="fa fa-cogs"> Name : <?php echo $_SERVER['SERVER_NAME']; ?> </span> 
         <br>
         <span class="fa fa-download"> Server : 
        <?php
        
        $x= $_SERVER['SERVER_SOFTWARE'];
        
        if (preg_match("/Apache/i", $x))
        {
            echo "Apache Server";
            
        } 
        else 
        {
            echo 'Server unknown';
        }
        ?> 
        </span>   
         <br>
         
         
        <span class="fa fa-asterisk"> Operating System : 
        <?php
        $y=$_SERVER['HTTP_USER_AGENT']; // to detect operating system so, i used HTTP_USER_AGENT
        if(preg_match('/windows/i', $y))
        {
            echo 'Windows';  
        }
        elseif(preg_match('/macintosh/i', $y))
        {
            echo 'Macintosh/';  
        }
        elseif(preg_match('/Linux/i', $y))
        {
            echo 'Linux';  
        }
       else
        {
            echo 'Unknown';  
        }
        ?> 
    
        </span> 
         <br>
        <span class="fa fa-bug"> Browser : 
        <?php
        $z=$_SERVER['HTTP_USER_AGENT']; // to detect Browser type so, i used HTTP_USER_AGENT
        if(preg_match('/firefox/i', $z))
        {
            echo 'Firefox';  
        }
        elseif(preg_match('/chrome/i', $z))
        {
            echo 'Chrome/';  
        }
        elseif(preg_match('/opera/i', $z))
        {
            echo 'Opera';  
        }
       else
        {
            echo 'Unknown';  
        }
        ?> 
    
        </span> 